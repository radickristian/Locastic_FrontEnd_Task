( function( $ ) {
	'use strict';
	$( '.hero-slider' ).slick( {
		fade: true,
		dots: true,
		arrows: false
	 } );

	 $( '.offcanvas-toggle' ).on( 'click', function() {
		$( 'body' ).toggleClass( 'offcanvas-expanded' );
		} );
	  $( '.offcanvas li a' ).on( 'click', function() {
		  $( 'body' ).removeClass( 'offcanvas-expanded' );
	  } );
	  $( '.offcanvas-toggle' ).on( 'click', function() {
		$( this ).toggleClass( 'active' );
	} );
}( jQuery ) );
